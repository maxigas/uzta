#!/usr/bin/python3
# Saves a Lorea seed a.k.a. the missing export module.
# Exit codes:
# 1 Failed to log in to the website.
# 2 Tokens not found.
# 3 Unexpected exception

__version__ = '2.0.4'

from argparse import ArgumentParser
from bs4 import BeautifulSoup as bs
from collections import OrderedDict
from datetime import datetime
from itertools import repeat
from joblib import Parallel, delayed
from pprint import pprint
from re import search, findall
from requests import get, post, Session
from time import sleep
from urllib.parse import urlsplit
from uuid import uuid4
from zipfile import ZipFile as zf
import os
import shutil
import yaml

# Custom errors

class LoginError(Exception):
        pass


class TokenError(Exception):
        pass


# Helper functions

# Patching pyaml to support OrderedDict:
# Problem: http://pyyaml.org/ticket/29
# Solution: http://stackoverflow.com/questions/5121931/in-python-how-can-you-load-yaml-mappings-as-ordereddicts
def ordered_dump(data, stream=None, Dumper=yaml.Dumper, **kwds):
    class OrderedDumper(Dumper):
        pass
    def _dict_representer(dumper, data):
        return dumper.represent_mapping(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            data.items())
    OrderedDumper.add_representer(OrderedDict, _dict_representer)
    return yaml.dump(data, stream, OrderedDumper, default_flow_style=False, allow_unicode=True, **kwds)
# usage:
# ordered_dump(data, Dumper=yaml.SafeDumper)

def mkdir(dir_name):
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)


def zipdir(path, zipfile_handle):
    for root, dirs, files in os.walk(path):
        for file in files:
            zipfile_handle.write(os.path.join(root, file))


def myeval(x):
    '''Helper function necessary for Python3 delayed(eval()...).'''
    eval(x)


def slurp_links(website, gid, query_string, is_group, s):
    """
    Takes group uid and type of content
    and return links from search query.
    (guid, type) => links
    """
    fork_parameter = '&container_guid=' if is_group else '&owner_guid='
    soup = bs(s.get(website+'search?q=%&entity_type=object&entity_subtype=' + query_string + fork_parameter + gid + '&limit=0&search_type=entities').text, 'html5lib')
    links = [x['href'] for x in soup.select('.elgg-body .elgg-list li .elgg-image-block .elgg-body .mbn a')]
    print('Found: ' + str(len(links)) + ' links of type: ' + query_string)
    return links


def lorea_session(username, password, website, with_request=False):
    s = Session()
    # ---- Get more tokens from front page
    print('---- Getting more tokens from front page')
    soup = bs(s.get(website).text, 'html5lib')
    try:
        tokens = [x['value'] for x in soup(class_='elgg-form-login')[0].find_all('input', type='hidden') if x['value'] != 'true' ]
    except:
        raise TokenError("Failed to find tokens on "+ website)
    data = {
            '__elgg_token':    tokens[0],
            '__elgg_ts':       tokens[1],
            'username':        username,
            'password':        password,
            'persistent':      'true',
            'returntoreferer': 'false'
    }
    # ---- Log in
    print('---- Logging in')
    post_url = website + 'action/login'
    r = s.post(post_url, data=data, allow_redirects=True)
    login_errors = bs(r.text, 'html5lib').select('.elgg-page-messages .elgg-system-messages .elgg-state-error')
    if len(login_errors) > 0:
        raise LoginError("Wrong username or password")
    if with_request:
        return s,r
    else:
        return s


# Group read functions

def read_group_decisions(url, s):
    """
    Take url and return decision string from
    decision page. url => decision string
    """
    print('Original decision URL: ' + url)
    soup = bs(s.get(url).text, 'html5lib')
    decision_title = soup.select('h2')[0].text
    decision_author = soup.select('.elgg-image-block .elgg-body .elgg-subtext a')[0].text
    decision_content = soup.select('.elgg-main .elgg-output')[0].text
    try:
        decision_proposal_author = soup.select('.crud-children .elgg-image-block .elgg-body .elgg-subtext a')[0].text
        decision_proposal_content = soup.select('.crud-children .elgg-output')[0].text
    except:
        decision_proposal_author = ''
        decision_proposal_content = ''
    decision_comments = soup.select('.decision-comments .elgg-list li .mbn')
    decision_comment_authors = [x.select('a')[0].text for x in decision_comments]
    decision_comment_contents = [x.select('.elgg-output')[0].text for x in decision_comments]
    ordered_comments = OrderedDict([OrderedDict([('author', a), ('comment', c)]) for a, c in zip(decision_comment_authors, decision_comment_contents)])
    return OrderedDict([('title', decision_title),
                        ('author', decision_author),
                        ('content', decision_content),
                        ('proposal_author', decision_proposal_author),
                        ('proposal_content', decision_content),
                        ('comments', ordered_comments)])


def read_group_assemblies(url, s):
    """
    Take url and return assembly from
    assembly page. url => assembly
    """
    print('Original assembly URL: ', url)
    soup = bs(s.get(url).text, 'html5lib')
    assembly_title = soup.select('h2')[0].text
    assembly_author = soup.select('.elgg-main .elgg-body .elgg-subtext a')[0].text
    try:
        assembly_date = soup.select('.elgg-main .elgg-body .elgg-subtext div')[0].text.split()[-1].split(':')[-1]
    except:
        assembly_date = 'unknown'
    decision_links = [x['href'] for x in soup.select('.crud-children .list-decision li .elgg-body h3 a')]
    decisions = [read_group_decisions(link, s) for link in decision_links]
    assembly_comments = soup.select('.assembly-comments .elgg-list li .mbn')
    comment_authors = [x.select('a')[0].text for x in assembly_comments]
    comment_contents = [x.select('.elgg-output')[0].text for x in assembly_comments]
    ordered_comments = OrderedDict([OrderedDict([('author', a), ('comment', c)]) for a, c in zip(comment_authors, comment_contents)])
    return OrderedDict([('title', assembly_title),
                        ('author', assembly_author),
                        ('date', assembly_date),
                        ('decisions', decisions),
                        ('comments', ordered_comments)])


def read_group_blog_entries(url, s):
    """
    Take url and return blog entry from
    blog entry page. url => blog entry
    """
    print('Original blog entry URL: ', url)
    soup = bs(s.get(url).text, 'html5lib')
    bentry_title = soup.select('h2')[0].text
    bentry_author = soup.select('.elgg-subtext a')[0].text
    # For new and old elgg
    try:
        bentry_timestamp = soup.select('.elgg-friendlytime acronym')[0]['title']
    except:
        bentry_timestamp = soup.select('.timestamp')[0]['datetime']
    bentry_content = soup.select('.blog-post')[0].text
    return OrderedDict([('title', bentry_title),
                        ('author', bentry_author),
                        ('timestamp', bentry_timestamp),
                        ('content', bentry_content)])


def read_group_bookmarks(url, s):
    """
    Take url and return bookmark from
    bookmark page. url => bookmark
    """
    print('Original bookmark URL: ', url)
    soup = bs(s.get(url).text, 'html5lib')
    bookmark_title = soup.select('h2')[0].text
    bookmark_author = soup.select('.elgg-subtext a')[0].text
    # For new and old elgg
    try:
        bookmark_timestamp = soup.select('.elgg-friendlytime acronym')[0]['title']
    except:
        bookmark_timestamp = soup.select('.timestamp')[0]['datetime']
    bookmark_link = soup.select('.elgg-heading-basic a')[0].get('href', "Link not found")
    return OrderedDict([('title', bookmark_title),
                        ('author', bookmark_author),
                        ('timestamp', bookmark_timestamp),
                        ('link', bookmark_link)])


def read_group_videos(url, s):
    """
    Take url and return video from
    video page. url => video
    """
    print('Original video URL: ', url)
    soup = bs(s.get(url).text, 'html5lib')
    video_title = soup.select('h2')[0].text
    video_author = soup.select('.elgg-subtext a')[0].text
    # For new and old elgg
    try:
        video_timestamp = soup.select('.elgg-friendlytime acronym')[0]['title']
    except:
        video_timestamp = soup.select('.timestamp')[0]['datetime']
    video_link = soup.select('iframe')[0]['src']
    return OrderedDict([('title', video_title),
                        ('author', video_author),
                        ('timestamp', video_timestamp),
                        ('link', video_link)])


def read_group_discussions(url, s):
    """
    Take url and return discussions from
    discussions page. soup => entries
    """
    print('--- Original discussion URL: ' + url)
    soup = bs(s.get(url).text, 'html5lib')
    entry_title = [x for x in soup.select('h2')][0].text
    entry_link = url
    # For new and old elgg
    try:
        entry_timestamp = [x['title'] for x in soup.select('.elgg-subtext span acronym') if not 'groups-latest-reply' in x.parent.parent['class']][0]
    except:
        entry_timestamp = [x['datetime'] for x in soup.select('.timestamp') if not 'groups-latest-reply' in x.parent.parent['class']][0]
    entry_author = [x for x in soup.select('.elgg-body .elgg-subtext a')][0].text
    try:
        entry_content = [x for x in soup.select('.elgg-content .elgg-output')][0].text
    except:
        entry_content = [x for x in soup.select('.elgg-layout .elgg-output')][0].text
    entry_replies = soup.select('.replies .elgg-list li .mbn')
    reply_authors = [x.select('a')[0].text for x in entry_replies]
    reply_contents = [x.select('.elgg-output')[0].text for x in entry_replies]
    # Not sure if we should enumerate the replies:
    # ordered_replies = [OrderedDict([('reply'+str(n), OrderedDict([('author', a), ('content', c)]))]) for a, c, n in zip(reply_authors, reply_contents, range(len(reply_authors)))]
    ordered_replies = [OrderedDict([('author', a), ('content', c)]) for a, c in zip(reply_authors, reply_contents)]
    return OrderedDict([
        ('title', entry_title),
        ('link', entry_link),
        ('author', entry_author),
        ('timestamp', entry_timestamp),
        ('content', entry_content),
        ('replies', ordered_replies)])


def read_group_tasks(url, s):
    """
    Take url and return tasks from
    task page. soup => tasks
    """
    print('Original task URL: ' + url)
    soup = bs(s.get(url).text, 'html5lib')
    task_title = soup.title.text.split(': ')[1]
    # task_link = [x.get('href').replace('?view=rss','') for x in soup.find_all('a') if x.get('title') == 'RSS feed for this page'][0]
    task_author = [x.text for x in soup.select('.elgg-image-block .elgg-body .elgg-subtext a')][0]
    task_done = 'unknown'
    task_tags = [tag.text for tag in soup.select('.elgg-tag')]
    task_content = soup.select('.elgg-output')[0].text
    return OrderedDict([
        ('title', task_title),
        ('link', url),
        ('author', task_author),
        ('done', task_done),
        ('tags', task_tags),
        ('content', task_content)])


def read_image_name_and_content(url, s):
    """
    Take url and return the image name and
    the content. url => (filename, content)
    """
    res = s.get(url)
    extension = res.headers['content-type'].split('/')[-1]
    id = url.split('/')[-3]
    filename = id + '.' + extension
    return (filename, res.content)


def read_group_photos(url, s):
    """
    Take url and return photos from
    photo page. url => photos
    """
    soup = bs(s.get(url).text, 'html5lib')
    website = "{0.scheme}://{0.netloc}".format(urlsplit(url))
    # Album properties are parsed from html
    photos_title = soup.title.text.split(': ')[1]
    photos_author = soup(class_='elgg-subtext')[0].find('a').text
    photos_tags = [tag.text for tag in soup.select('.elgg-tag')]
    # Photo properties are parsed from rss
    url = url+"?view=rss"
    soup = bs(s.get(url).text, features='xml')
    photos_titles = [x.text for x in soup.select('title')]
    photos_links = [x.text for x in soup.select('link')]
    photos_link = url
    photos_ids = [url.split('/')[-2] for url in photos_links if url is not '']
    photos_links = [website + "/photos/thumbnail/" + id + "/master/"
					for id in photos_ids]
    photos_contents = []
    photos_names = []
    for link in photos_links:
        filename, file_content = read_image_name_and_content(link, s)
        photos_contents.append(file_content)
        photos_names.append(filename)
    print ('- ' + str(len(photos_contents)) + ' photos found')
    return (OrderedDict([('title',     photos_title),
                         ('titles',    photos_titles),
                         ('author',    photos_author),
                         ('link',      photos_link),
                         ('links',     photos_links),
                         ('ids',       photos_ids),
                         ('filenames', photos_names),
                         ('tags',      photos_tags)]),
            photos_contents)


def read_file_name_and_content(url, s):
    """
    Take url and return a filename
    and content. url => (filename, content)
    """
    res = s.get(url)
    header = res.headers
    file_name = header['Content-Disposition'].split('=')[-1]. strip('"')
    content = res.content
    if file_name == '':
        return ('file_not_found'+str(uuid4())+'.txt',
                'The website returned an empty file when trying to retrieve this URL: %s' % url)
    return (file_name, content)


def read_group_files(url, s):
    """
    Take url and return a file from
    file page. url => file
    """
    print('Original file URL: ', url)
    soup = bs(s.get(url).text, 'html5lib')
    file_link = soup.select('.elgg-button-action')[0]['href']
    file_title = soup.title.text.split(': ')[1]
    file_author = soup.select('.elgg-subtext a')[0].text
    file_description = soup.select('.elgg-content .elgg-output')[0].text
    # For new and old elgg  
    try:
        file_timestamp = soup.select('.elgg-friendlytime acronym')[0]['title']
    except:
        file_timestamp = soup.select('.timestamp')[0]['datetime']
    file_name, file_content = read_file_name_and_content(file_link, s)
    return (OrderedDict([('title', file_title),
                        ('author', file_author),
                        ('description', file_description),
                        ('timestamp', file_timestamp),
                        ('link', file_link),
                        ('name', file_name)]),
            file_content)


def read_group_wiki_page(url, s):
    """
    Take url and return wiki page from
    wiki. url => wiki page
    """
    print('--- Reading wiki page: ' + url)
    soup = bs(s.get(url + '&do=edit').text, 'html5lib')
    title = soup.select('.pagename a')[0].text
    content = soup.select('textarea')[0].text
    return OrderedDict([('title', title),
                        ('content', content)])


def read_group_wiki(url, s):
    """
    Take url and return wiki pages from
    wiki. url => wiki pages
    """
    print("--- Original URL: " + url)
    wiki_half_links = findall('"/dokuwiki/[0-9]*/doku.php\?id=[\w]*"', s.get(url).text)
    # Remove duplicates
    wiki_half_links = list(set(wiki_half_links))
    # Forge full links
    website = "{0.scheme}://{0.netloc}".format(urlsplit(url))
    wiki_links = [website+x.replace('"', '') for x in wiki_half_links]
    wiki_pages = [read_group_wiki_page(link, s) for link in wiki_links]
    return wiki_pages


def read_group_pages(url, s):
    """
    Take url and return page from
    page page. url => page
    """
    print('Original page URL: ', url)
    soup = bs(s.get(url).text, 'html5lib')
    try:
        title = soup.select('h2')[0].text
        author = soup.select('.elgg-subtext a')[0].text
        # For new and old elgg
        try:
            timestamp = soup.select('.elgg-friendlytime acronym')[0]['title']
        except:
            timestamp = soup.select('.timestamp')[0]['datetime']
        content = soup.select('.elgg-content .elgg-output')[0].text
        page_comments = soup.select('.elgg-comments .elgg-list li .mbn')
        comment_authors = [x.select('a')[0].text for x in page_comments]
        comment_content = [x.select('.elgg-output')[0].text for x in page_comments]
        ordered_comments = OrderedDict([OrderedDict([('author', a), ('comment', c)]) for a, c in zip(comment_authors, comment_content)])
    except:
        # errors = wfnc_errors
        # errors += ["Uzta found a broken link on url: " + url]
        title = 'This was a broken page or broken pad'
        author = ''
        timestamp = ''
        content = ''
        ordered_comments = ''
    return OrderedDict([('title', title),
                        ('author', author),
                        ('timestamp', timestamp),
                        ('content', content),
                        ('comments', ordered_comments)])


# User read functions

def read_user_wires(url, s):
    """
    Take url and return wires from
    wires page. url => wires
    """
    print('Original wire URL: ', url)
    soup = bs(s.get(url).text, 'html5lib')
    wire_content = [x for x in soup.select('.elgg-body .elgg-content')][0].text
    wire_author = [x for x in soup.select('h2')][0].text.split(' ')[-1]
    # For new and old elgg
    try:
        wire_timestamp = [x['title'] for x in soup.select('.elgg-friendlytime acronym')][0]
    except:
        wire_timestamp = [x['datetime'] for x in soup.select('.timestamp')][0]
    return OrderedDict([
        ('author', wire_author),
        ('timestamp', wire_timestamp),
        ('content', wire_content)])


def read_user_blog_entries(url, s):
    return read_group_blog_entries(url, s)


def read_user_bookmarks(url, s):
    return read_group_bookmarks(url, s)


def read_user_videos(url, s):
    return read_group_videos(url, s)


def read_user_files(url, s):
    return read_group_files(url, s)


def read_user_photos(url, s):
    return read_group_photos(url, s)


def read_user_tasks(url, s):
    return read_group_tasks(url, s)


def read_user_pages(url, s):
    return read_group_pages(url, s)


# Group write functions

def write_group_assemblies(assemblies, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'assemblies',
                                'assembly')
    for assembly, n in zip(assemblies, range(len(assemblies))):
        filename = filebasename+str(n)+'.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(assembly, fileio)          


def write_group_discussions(entries, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'discussions',
                                'discussion')
    for entry, n in zip(entries, range(len(entries))):
        filename = filebasename+str(n)+'.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(entry, fileio)


def write_group_tasks(tasks, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'tasks',
                                'task')
    for task, n in zip(tasks, range(len(tasks))):
        filename = filebasename+str(n)+'.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(task, fileio)

    
def write_group_photos(album_tuples, output_dir_name, dir_name):
    basename = os.path.join(output_dir_name, dir_name, 'photos')
    albums = [album_tuple[0] for album_tuple in album_tuples]
    album_streams = [album_tuple[1] for album_tuple in album_tuples]
    for album, photo_streams, n in zip(albums, album_streams, range(len(albums))):
        # Replace slashes in photo album directory names:
        file_basepath = os.path.join(basename, album['title'].replace('/', ''))
        mkdir(file_basepath)
        for filename, photo_stream in zip(album['filenames'], photo_streams):
            with open(os.path.join(file_basepath, filename), 'wb') as fileio:
                fileio.write(photo_stream)
        with open(os.path.join(basename, 'album'+str(n)+'.txt'), 'w') as fileio:
            ordered_dump(album, fileio)


def write_group_files(file_tuples, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'files',
                                'file')
    diskfilebasename = os.path.join(output_dir_name,
                                    dir_name,
                                    'files',
                                    'static_files')
    files = [file_tuple[0] for file_tuple in file_tuples]
    streams = [file_tuple[1] for file_tuple in file_tuples]
    for f, s, n in zip(files, streams, range(len(files))):
        diskfilename = os.path.join(diskfilebasename, f['name'])
        with open(diskfilename, 'wb') as handle:
            try:
                handle.write(s)
            except:
                handle.write(bytes(s, 'utf-8'))
        filename = filebasename+str(n)+'.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(f, fileio)


def write_group_videos(videos, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'videos',
                                'video')
    for video, n in zip(videos, range(len(videos))):
        filename = filebasename+str(n)+'.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(video, fileio)


def write_group_bookmarks(bookmarks, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'bookmarks',
                                'bookmark')
    for bm, n in zip(bookmarks, range(len(bookmarks))):
        filename = filebasename+str(n)+'.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(bm, fileio)


def write_group_blog_entries(blog_entries, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'blog',
                                'entry')
    for be, n in zip(blog_entries, range(len(blog_entries))):
        filename = filebasename+str(n)+'.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(be, fileio)


def write_group_wiki(wiki_pages_list, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'wiki',
                                'page')
    try:
        wiki_pages = wiki_pages_list[0]
    except:
        wiki_pages = []
    for page, n in zip(wiki_pages, range(len(wiki_pages))):
        filename = filebasename+str(n)+'.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(page, fileio)


def write_group_decisions(decisions, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'decisions',
                                'decision')
    for decision, n in zip(decisions, range(len(decisions))):
        filename = filebasename+str(n)+'.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(decision, fileio)


def write_group_pages(pages, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'pages',
                                'page')
    for page, n in zip(pages, range(len(pages))):
        filename = filebasename + str(n) + '.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(page, fileio)


# User writing functions

def write_user_wires(wires, output_dir_name, dir_name):
    filebasename = os.path.join(output_dir_name,
                                dir_name,
                                'wires',
                                'wire')
    for wire, n in zip(wires, range(len(wires))):
        filename = filebasename + str(n) + '.txt'
        with open(filename, 'w') as fileio:
            ordered_dump(wire, fileio)


def write_user_blog_entries(entries, output_dir_name, dir_name):
    write_group_blog_entries(entries, output_dir_name, dir_name)


def write_user_bookmarks(bookmarks, output_dir_name, dir_name):
    write_group_bookmarks(bookmarks, output_dir_name, dir_name)


def write_user_videos(videos, output_dir_name, dir_name):
    write_group_videos(videos, output_dir_name, dir_name)


def write_user_files(files, output_dir_name, dir_name):
    write_group_files(files, output_dir_name, dir_name)


def write_user_photos(photos, output_dir_name, dir_name):
    write_group_photos(photos, output_dir_name, dir_name)


def write_user_tasks(tasks, output_dir_name, dir_name):
    write_group_tasks(tasks, output_dir_name, dir_name)

def write_user_pages(pages, output_dir_name, dir_name):
    write_group_pages(pages, output_dir_name, dir_name)


def uzta(username, password, website, version, output_dir_name="output", wants_zip=False, zipout=".", jobs=-1):
    """
    Uzta main function, where the magic happens.
    """

    # ---- Step 0. Initialisation
    print('---- Step 0. Initialisation')

    # starting datetime
    starting_at = datetime.now()
    
    website = website if website[-1] == '/' else website + '/'
    zipout = zipout if zipout[-1] == '/' else zipout + '/'

    #  Workflow Non Critical Errors
    # wfnc_errors = []

    # ---- Step 1. and 2. Get tokens from front page and log in
    try:
        s,r = lorea_session(username, password, website, with_request=True)
    except LoginError as e:
        return str(e)
    except TokenError as e:
        return str(e)
    except Exception as e:
        return str(e)

    # ---- Step 3. Goto groups page and get group list
    print('---- Step 3. Goto groups page and get group list')
    soup = bs(s.get(website + 'groups/member/' + username + '/').text, 'html5lib')
    groupsoup = soup(class_='elgg-list')[0]
    grouplinksoup = [x.find_all('a') for x in groupsoup.find_all('h3')]
    # TODO watchout for code injection from group names
    group_titles = [x[0].get_text().replace('/', '') for x in grouplinksoup]
    group_ids = [x['id'].split('-')[-1] for x in soup.select('.elgg-list-entity .elgg-item')]
    groups = list(map(lambda t, i: {'title': t, 'id': i},
                      group_titles, group_ids))

    # ---- Step 4. Retrieve user GUID
    print('---- Step 4. Retrieving user GUID')
    user_gid = findall('"guid":[0-9]*', str(r.content))[0].split(':')[-1]

    # ---- Step 5. Prepare user output folder structure
    print('---- Step 5. Preparing output folder structure')
    mkdir(output_dir_name)
    mkdir(os.path.join(output_dir_name, username))
    mkdir(os.path.join(output_dir_name, username, 'wires'))
    mkdir(os.path.join(output_dir_name, username, 'blog'))
    mkdir(os.path.join(output_dir_name, username, 'bookmarks'))
    mkdir(os.path.join(output_dir_name, username, 'videos'))
    mkdir(os.path.join(output_dir_name, username, 'files'))
    mkdir(os.path.join(output_dir_name, username, 'files', 'static_files'))
    mkdir(os.path.join(output_dir_name, username, 'photos'))
    mkdir(os.path.join(output_dir_name, username, 'tasks'))
    mkdir(os.path.join(output_dir_name, username, 'pages'))

    # ---- Step 6. Prepare groups output folder structure
    print('---- Step 6. Preparing groups output folder strucutre')
    for group in groups:
        mkdir(os.path.join(output_dir_name, group['title']))
        mkdir(os.path.join(output_dir_name, group['title'], 'discussions'))
        mkdir(os.path.join(output_dir_name, group['title'], 'tasks'))
        mkdir(os.path.join(output_dir_name, group['title'], 'photos'))
        mkdir(os.path.join(output_dir_name, group['title'], 'files'))
        mkdir(os.path.join(output_dir_name, group['title'], 'files', 'static_files'))
        mkdir(os.path.join(output_dir_name, group['title'], 'videos'))
        mkdir(os.path.join(output_dir_name, group['title'], 'bookmarks'))
        mkdir(os.path.join(output_dir_name, group['title'], 'blog'))
        mkdir(os.path.join(output_dir_name, group['title'], 'wiki'))
        mkdir(os.path.join(output_dir_name, group['title'], 'decisions'))
        mkdir(os.path.join(output_dir_name, group['title'], 'pages'))
        mkdir(os.path.join(output_dir_name, group['title'], 'assemblies'))

    # ---- Step 7. Harvest group ontology
    print('---- Step 7. Harvesting group ontology')

    # Type Codes for Search API:
    # 'file', 'page', 'assembly', 'videolist_item', 'bookmarks', 'task'
    # 'groupforumtopic'(discussion), 'album', 'blog', 'dokuwiki'

    group_ontology_to_query_strings = {
            'assemblies': 'assembly',
            'discussions': 'groupforumtopic',
            'tasks': 'task',
            'photos': 'album',
            'files': 'file',
            'videos': 'videolist_item',
            'bookmarks': 'bookmarks',
            'blog_entries': 'blog',
            'pages': 'page',
            'wiki': 'dokuwiki'
    }

    # Production version, parallelized
    for group, n in zip(groups, range(len(groups))):
        print("--- Step 7."+str(n)+" Harvesting "+group['title']+ " group")
        Parallel(n_jobs=jobs, backend="threading")(delayed(myeval) \
                                  ("write_group_%s(list(map(read_group_%s, slurp_links('%s', '%s', '%s', %s, lorea_session('%s', '%s', '%s')), repeat(lorea_session('%s', '%s', '%s')))), '%s', '%s')" \
                                   % (x, x, website, group['id'], y, 'True',
                                      username, password, website,
                                      username, password, website,
                                      output_dir_name, group['title'])) \
                                  for x, y in group_ontology_to_query_strings.items())

    # ---- Step 8. Harvest user ontology
    print('---- Step 8. Harvesting user ontology')

    # Type Codes for Search API:
    # 'file', 'page', 'thewire', 'videolist_item', 'bookmarks', 'task'
    # 'album', 'blog'

    user_ontology_to_query_strings = {
            'wires': 'thewire',
            'tasks': 'task',
            'photos': 'album',
            'files': 'file',
            'videos': 'videolist_item',
            'bookmarks': 'bookmarks',
            'blog_entries': 'blog',
            'pages': 'page'
    }

    Parallel(n_jobs=jobs, backend="threading")(delayed(myeval) \
                          ("write_user_%s(list(map(read_user_%s, slurp_links('%s', '%s', '%s', %s, lorea_session('%s', '%s', '%s')), repeat(lorea_session('%s', '%s', '%s')))), '%s', '%s')" \
                           % (x, x, website, user_gid, y, 'False',
                              username, password, website,
                              username, password, website,
                              output_dir_name, username)) \
                          for x, y in user_ontology_to_query_strings.items())
    # ---- Step 9. Create README.md file
    print('---- Step 9. Creating README file')

    # ending datetime
    ending_at = datetime.now()
    
    # number of harvested files
    file_count = sum([len(files) for root, dirs, files in os.walk(output_dir_name)])
    lines = [
            "# README",
            "Thanks for using `uzta`. In this file you will find information \
about the process as well as the specific version of the software that \
was used. If you're planning to use an import script to upload your data \
to another platform, you would like to keep this file as it is without \
changing it or deleting it.",
            "",
            "## About uzta",
            "Version: "+version,
            "",
            "## About harvesting",
            "Started at: "+str(starting_at),
            "Ended at: "+str(ending_at),
            "Elapsed time: "+str(ending_at-starting_at),
            "Username: "+username,
            str(file_count)+" files harvested"
            ]
    filename = os.path.join(output_dir_name, 'README.md')
    with open(filename, 'w') as handle:
        for line in lines:
            print(line, file=handle)

    # ---- Step 10. Print Workflow Non Critical Errors
    # print('---- Step 10. Printing Workflow Non Critical Errors')
    # if len(wfnc_errors) > 0:
    #    print("Uzta found the following non critical errors during the harvest:")
    #    pprint(wfnc_errors)

    # ---- Step 11. Create zip file
    if wants_zip:
        print("---- Step 11. Creating zip file")
        if zipout != '.':
            mkdir(zipout)
            zipfile = zf(zipout + "uzta.zip", 'w')
        else:
            zipfile = zf("uzta.zip", 'w')
        zipdir(output_dir_name, zipfile)
        zipfile.close
        # Brutally remove temporary files
        print("--- Almost done: brutally remove temporary files")
        shutil.rmtree(output_dir_name)

    print("Uzta is done!")
    return None

if __name__ == '__main__':

    # Argument parser initialization
    parser = ArgumentParser()
    parser.add_argument("username", help="Your username")
    parser.add_argument("password", help="Your password")
    parser.add_argument("website", help="The full URL of the lorea seed you want to harvest.")
    parser.add_argument("-o", "--output", dest="output", default="output", help="The directory name to store the data output. If doesn't exist, it will be created. Default is 'output'.")
    parser.add_argument("-z", "--zip", dest="wants_zip", action="store_true", help="The result of the harvesting will be a uzta.zip file. The output directory will be treated as a temp dir and will be permanently deleted!")
    parser.add_argument("-d", "--zipout", dest="zipout", default='.', help="The directory name to store the zip file. If doesn't exist, it will be created. Default is 'output'.")
    parser.add_argument("-j", "--jobs", type=int, dest="jobs", default=-1, help="Number of parallel CPU cores to use. Default is all CPUs.")
    parser.add_argument("-t", "--debug", dest="debug", action="store_true", help="Debug mode. Print traceback on errors. Useful for reporting bugs to developers. Default is False.")
    args = parser.parse_args()

    # Harvesting with uzta
    error = uzta(args.username, args.password, args.website, __version__,
             output_dir_name=args.output, wants_zip=args.wants_zip, jobs=args.jobs)
    print("Success!") if not error else print(error) if args.debug else print("An unexpected error ocurred")
    exit(0)
